/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.fileinputout;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Arthit
 */
public class TestFileOutput {

    public static void main(String[] args) {
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        Rectangle rec =null;
        try {
            Dog dog = new Dog("JeeJa", 50);
            rec = new Rectangle(4,10);
            File file = new File("Dog.obj");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(dog);
            oos.writeObject(rec);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            System.out.println("Not file");
        } catch (IOException ex) {
            Logger.getLogger(TestFileOutput.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
