/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.fileinputout;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Arthit
 */
public class TestInput {
    public static void main(String[] args) {
         FileInputStream fis = null;
         ObjectInputStream ois=null;
         Rectangle rec;
         Dog dog =null;
         Rectangle rec2;
        try {
            File file = new File ("Dog.obj");
            rec = new Rectangle(4,10);
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            dog = (Dog)ois.readObject();
            rec2 = (Rectangle)ois.readObject();
            System.out.println(dog);
            System.out.println(rec2);
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            System.out.println("not file");
        } catch (IOException ex) {
            Logger.getLogger(TestInput.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TestInput.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
